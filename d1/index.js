// In JavaScript, each element in a webpage (document) such as images and text inputs can be considered an object.
/*

The document refers to the whole webpage and querySelector is used to select a specific object (HTML element) from the document (webpage).

document.querySelector("#txt-first-name");
document.getElementById("txt-inputs");
document.getElementsByTagName("input");

*/
console.log(document);
console.log(document.querySelector("#txt-first-name"));
// result - element from html
/* 
    document - refers to the whole webpage
    querySelector - used to select a specific element (object) as long as it is inside the html tag(HTML ELEMENT);
    -takes a string input that is formatted like CSS Selector
    -can select elements regardless if the string is an id, a class, or a tag as long as the element is existing in the webpage.

*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullname = document.querySelector("#span-full-name");

/* 
    event - actions that the user is doing in our webpage (scroll, click, hover, keypress/type)

    addEventListener - function that lets the webpage to listen to the events performed by the user
        - takes two arguments
            - string - the event to which the HTML element will listen, these are predetermined
            - function - executed by the element once the event (first argument) is triggered

*/

txtFirstName.addEventListener("keyup", (event) => {
    spanFullname.innerHTML = txtFirstName.value //innerHTML the space between <span> and </span>
});

/* txtFirstName.addEventListener("keyup", (event) => {
    console.log(event);
    console.log(event.target);
    console.log(event.target.value);
}); */

/* 
    Make another keyup event where in the span element will record the last name in the forms
    send the output in the batch google chat

*/

txtLastName.addEventListener("keyup", (event) => {
    spanFullname.innerHTML = txtLastName.value
});

/* 
    Event Listeners
        change - An element has been changed (e.g. input value).
        click - An elemnt has been clicked (e.g. button clicks).
        load - The browser has finished loading a webpage.
        keydown - A keyboard key is pushed.
        keyup - A keyboard key is released after being pushed.

*/